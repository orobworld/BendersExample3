package threadsafe;

import static bendersexample.Demo.CALLS;
import bendersexample.Model;
import bendersexample.Problem;
import bendersexample.Solution;
import ilog.concert.IloException;
import ilog.cplex.IloCplex.CplexStatus;
import java.io.OutputStream;

/**
 * ManualBenders2 provides an alternative version of manual Benders
 * decomposition, using thread local objects to hold separate copies of
 * the subproblem for each thread.
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class ManualBenders2 implements Model {
  private final Problem problem;       // the problem being solved
  private final MasterProblem master;  // the master problem model
  private OutputStream subout;         // the subproblem output stream
  private int seed;                    // random seed for master and subproblems
  private final boolean showCuts;      // show cut details?
  private boolean showCalls;           // show callback entry messages?

  /**
   * Constructor.
   * @param prob the problem to solve
   * @param sc if true, show cut details
   * @param rs random seed for the solvers
   * @throws IloException if the master problem cannot be constructed
   */
  public ManualBenders2(final Problem prob, final boolean sc, final int rs)
         throws IloException {
    problem = prob;
    seed = rs;
    showCuts = sc;
    showCalls = false;
    // Create the master MIP.
    master = new MasterProblem(problem);
  }

  /**
   * Set the Benders strategy.
   *
   * This operation is unsupported for the manual method.
   *
   * @param strategy the new strategy
   * @throws IloException if setting the strategy fails.
   */
  @Override
  public void setBendersStrategy(final int strategy) throws IloException {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  /**
   * Solve the model.
   * @return the solution (in an instance of Solution)
   * @throws IloException if CPLEX encounters problems
   */
  @Override
  public Solution solve() throws IloException {
    // Create a Benders callback and attach it to the master problem.
    BendersCallback2 callback =
      new BendersCallback2(problem, master, subout, seed, showCuts, showCalls);
    master.attach(callback);
    // Solve the master problem.
    Solution sol = master.solve();
    // If the master problem found a solution, add in the values of the flow
    // variables (stored in the callback).
    CplexStatus status = sol.getStatus();
    if (status == CplexStatus.Optimal || status == CplexStatus.Feasible) {
      callback.addFlows(sol);
    }
    return sol;
  }

  /**
   * Set the output level for the solver.
   *
   * @param level the desired output level
   * (0 = none [default], 1 = master only, 2 = master and subproblems,
   * plus Demo.CALLS if callback entry messages are requested)
   */
  @Override
  public void setOutputLevel(final int level) {
    // Show callback entry messages?
    showCalls = (level >= CALLS);
    // Set the master problem output level and record the subproblem output
    // level (to be used whenever a new copy of the subproblem is created).
    switch (level % CALLS) {
      case 0:  // suppress all output
        master.setOut(null);
        subout = null;
        break;
      case 1:  // master output only
        master.setOut(System.out);
        subout = null;
        break;
      case 2:  // master and subproblem output (which is just master in
               // this case)
        master.setOut(System.out);
        subout = System.out;
        break;
      default: // illegal argument
        throw new IllegalArgumentException("Invalid output level specified.");
    }
  }

  /**
   * Set the random number seed for the solver.
   * @param rseed the seed to use
   * @throws IloException if the seed cannot be set
   */
  @Override
  public void setSeed(final int rseed) throws IloException {
    // Set the master problem seed and record the subproblem seed
    // (to be used whenever a new copy of the subproblem is created).
    master.setSeed(rseed);
    seed = rseed;
  }

  /**
   * Clean up any objects attached to the model.
   */
  @Override
  public void end() {
    master.end();
  }
}
