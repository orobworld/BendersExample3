package threadsafe;

import bendersexample.Demo;
import bendersexample.Problem;
import bendersexample.Solution;
import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import java.io.OutputStream;
import java.util.HashSet;

/**
 * MasterProblem holds the master problem for the ManualBenders2 decomposition.
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MasterProblem {
  private final int nWarehouses;       // number of warehouses
  private final int nCustomers;        // number of customers
  private final IloCplex master;       // the master model
  private final IloNumVar[] use;       // binary variables selecting warehouses
  private final IloNumVar flowCost;    // surrogate variable for flow costs

  /**
   * Constructor.
   * @param problem the problem being solved
   * @throws IloException if the master problem cannot be constructed
   */
  public MasterProblem(final Problem problem)
         throws IloException {
    nWarehouses = problem.getWarehouseCount();
    nCustomers = problem.getCustomerCount();
    // Get the fixed costs.
    double[] fixed = new double[nWarehouses];
    for (int w = 0; w < nWarehouses; w++) {
      fixed[w] = problem.getFixedCost(w);
    }
    // Initialize the master problem.
    master = new IloCplex();
    // Set up the master problem (which initially has no constraints).
    String[] names = new String[nWarehouses];
    for (int w = 0; w < nWarehouses; w++) {
      names[w] = "Use_" + w;
    }
    use = master.boolVarArray(nWarehouses, names);
    flowCost = master.numVar(0.0, Double.MAX_VALUE, "estFlowCost");
    master.addMinimize(master.sum(flowCost, master.scalProd(fixed, use)),
                       "TotalCost");
    // Suppress master problem output by default.
    master.setOut(null);
  }

  /**
   * Set the output stream for the master problem.
   * @param stream the new output stream
   */
  public void setOut(final OutputStream stream) {
    master.setOut(stream);
  }

  /**
   * Set the random seed to be used by the solver.
   * @param seed the new seed
   * @throws IloException is somehow, against all odds, the seed cannot be set
   */
  public void setSeed(final int seed) throws IloException {
    master.setParam(IloCplex.IntParam.RandomSeed, seed);
  }

  /**
   * Clean up all CPLEX objects.
   */
  public void end() {
    master.end();
  }

  /**
   * Generate a linear expression with a specified constant term.
   * The intent is to use this expression in creating Benders cuts for this
   * master problem.
   * @param constant the constant term
   * @return the linear expression
   * @throws IloException if the expression cannot be generated
   */
  public IloLinearNumExpr expression(final double constant)
         throws IloException {
    return master.linearNumExpr(constant);
  }

  /**
   * Generate a term for a linear expression.
   * @param coefficient the coefficient of the term
   * @param variableIndex the index of the binary variable in the term
   * @return the term
   * @throws IloException if CPLEX has any objections
   */
  public IloLinearNumExpr term(final double coefficient,
                               final int variableIndex)
                          throws IloException {
    IloLinearNumExpr expr = master.linearNumExpr();
    expr.addTerm(coefficient, use[variableIndex]);
    return expr;
  }

  /**
   * Attach a generic callback to the master problem.
   * The callback should be invoked only when an allegedly feasible solution
   * is found or when a thread is started or ended.
   * @param callback the callback to attach
   * @throws IloException if CPLEX balks at attaching the callback
   */
  public void attach(final BendersCallback2 callback) throws IloException {
    master.use(callback, IloCplex.Callback.Context.Id.Candidate
               | IloCplex.Callback.Context.Id.ThreadUp
               | IloCplex.Callback.Context.Id.ThreadDown);
  }

  /**
   * Solve the model.
   * @return the solution (in an instance of Solution)
   * @throws IloException if CPLEX encounters problems
   */
  public Solution solve() throws IloException {
    Solution s = new Solution(nWarehouses, nCustomers);
    long start = System.currentTimeMillis();
    if (master.solve()) {
      // Record the solution (other than the flows).
      s.setTime(System.currentTimeMillis() - start);
      s.setCost(master.getObjValue());
      s.setStatus(master.getCplexStatus());
      HashSet<Integer> warehouses = new HashSet<>();
      double[] u = master.getValues(use);
      for (int w = 0; w < nWarehouses; w++) {
        if (u[w] > Demo.ROUNDUP) {
          warehouses.add(w);
        }
      }
      s.setWarehouses(warehouses);
    } else {
      // Record only the solution time and status.
      s.setTime(System.currentTimeMillis() - start);
      s.setStatus(master.getCplexStatus());
    }
    return s;
  }

  /**
   * Get the value of the flow cost surrogate variable in a candidate solution.
   * @param context the context having the candidate solution
   * @return the value of the flow cost surrogate variable
   * @throws IloException if CPLEX balks
   */
  public double getFlowCost(final IloCplex.Callback.Context context)
                throws IloException {
    return context.getCandidatePoint(flowCost);
  }

  /**
   * Get the values of the warehouse decisions in a candidate solution.
   * @param context the context having the candidate solution
   * @return the vector of values of the use variables
   * @throws IloException if CPLEX balks
   */
  public double[] getWarehouseChoices(final IloCplex.Callback.Context context)
                throws IloException {
    return context.getCandidatePoint(use);
  }

  /**
   * Get the product of a numerical expression (from the master problem) and
   * a scalar coefficient.
   * @param coefficient the coefficient
   * @param expr the expression
   * @return the product of the coefficient and the expression
   * @throws IloException if CPLEX balks
   */
  public IloNumExpr product(final double coefficient, final IloNumExpr expr)
                          throws IloException {
    return master.prod(coefficient, expr);
  }

  /**
   * Generate a cut for the master problem.
   * @param expr the left side expression (must belong to the master problem)
   * @param rhs the right side constant
   * @param sense 'G', 'L' or 'E' for greater-equal/less-equal/equal
   * @return the cut
   * @throws IloException if CPLEX objects to something
   */
  public IloRange makeCut(final IloNumExpr expr, final double rhs,
                          final char sense) throws IloException {
    switch (sense) {
      case 'G':
      case 'g':
        return master.ge(expr, rhs);
      case 'L':
      case 'l':
        return master.le(expr, rhs);
      case 'E':
      case 'e':
        return master.eq(expr, rhs);
      default:
        throw new IllegalArgumentException("Unknown constraint sense " + sense);
    }
  }

  /**
   * Generate a cut for the master problem.
   * @param var the master problem variable on the left side of the constraint
   * @param expr the right side expression (must belong to the master problem)
   * @param sense 'G', 'L' or 'E' for greater-equal/less-equal/equal
   * @return the cut
   * @throws IloException if CPLEX objects to something
   */
  public IloRange makeCut(final IloNumVar var, final IloNumExpr expr,
                          final char sense) throws IloException {
    switch (sense) {
      case 'G':
      case 'g':
        return (IloRange) master.ge(var, expr);
      case 'L':
      case 'l':
        return (IloRange) master.le(var, expr);
      case 'E':
      case 'e':
        return (IloRange) master.eq(var, expr);
      default:
        throw new IllegalArgumentException("Unknown constraint sense " + sense);
    }
  }

  /**
   * Get the flow cost surrogate variable.
   * @return the flow cost variable
   */
  public IloNumVar getFlowCostVariable() {
    return flowCost;
  }

  /**
   * Add an expression to another one.
   * @param expr the first expression
   * @param expr2 the second expression
   * @return the sum
   * @throws IloException if CPLEX objects
   */
  public IloNumExpr add(final IloNumExpr expr, final IloNumExpr expr2)
                    throws IloException {
    return master.sum(expr, expr2);
  }
}
