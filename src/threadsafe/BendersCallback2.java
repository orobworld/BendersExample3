package threadsafe;

import bendersexample.Demo;
import bendersexample.Problem;
import bendersexample.Solution;
import ilog.concert.IloConstraint;
import ilog.concert.IloException;
import ilog.concert.IloNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import java.io.OutputStream;

/**
 * BendersCallback2 is a generic callback that tests proposed incumbent
 * solutions to the master problem and either passively accepts them or rejects
 * them by generating either an optimality cut or a feasibility cut.
 *
 * To avoid thread collisions, a separate copy of the subproblem is stored
 * for each thread. The callback function creates subproblem copies on an
 * as-needed basis when a thread without a copy starts up. During the solution
 * process, no threads modify the master problem, so a single global copy of
 * the master problem is sufficient.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class BendersCallback2 implements IloCplex.Callback.Function {
  private final int nWarehouses;       // number of warehouses
  private final int nCustomers;        // number of customers
  private final Problem problem;       // the problem being solved
  private final MasterProblem master;  // the master problem
  private final ThreadLocal<Subproblem> subproblem;  // the subproblem for this
                                                     // thread
  private final IloNumVar flowCost;    // master problem flow cost variable
  private final OutputStream subout;   // subproblem output stream
  private final int seed;              // subproblem random seed
  private final boolean showCuts;      // print cuts as they are generated?
  private final boolean showCalls;     // show callback entry messages?
  //
  // To avoid having to resolve the subproblem when a solution is found, we
  // store the values of the flow variables for the incumbent master solution.
  // Updating those values needs to be done in a thread-safe way. To make sure
  // that one thread does not overwrite the solution with flows from an
  // incumbent that is actually worse than one already found in another thread,
  // we also store the incumbent value of the master problem objective.
  //
  private double incumbentValue;       // master problem objective value
  private final double[][] flowValues; // subproblem flow values

  /**
   * Constructor.
   * @param p the problem being solved
   * @param m the master problem instance being used
   * @param os the output stream to use for subproblems
   * @param s the seed for the subproblem solver
   * @param cuts show cuts?
   * @param calls show callback entry messages?
   */
  public BendersCallback2(final Problem p, final MasterProblem m,
                          final OutputStream os, final int s,
                          final boolean cuts, final boolean calls) {
    problem = p;
    subout = os;
    master = m;
    nWarehouses = problem.getWarehouseCount();
    nCustomers = problem.getCustomerCount();
    flowValues = new double[nWarehouses][nCustomers];
    subproblem = new ThreadLocal<>();
    seed = s;
    showCuts = cuts;
    showCalls = calls;
    flowCost = master.getFlowCostVariable();
    incumbentValue = Double.POSITIVE_INFINITY;
  }

    /**
     * When called from CPLEX, determine the context of the call. If an
     * allegedly feasible master solution has been found, solve the
     * corresponding subproblem and generate cuts as needed. If a thread is
     * going live, initialize its memory (create a copy of the subproblem,
     * if needed). If a thread is shutting down, clear the stored subproblem.
     * Being called from any other context is an error.
     *
     * There is some redundant code deliberately added here, for test purposes.
     *
     * @param context the context of the call
     * @throws IloException if the cut generation subproblem throws an error
     */
  @Override
  public void invoke(final IloCplex.Callback.Context context)
              throws IloException {
    int id = context.getIntInfo(IloCplex.Callback.Context.Info.ThreadId);
    int nth = context.getIntInfo(IloCplex.Callback.Context.Info.Threads);
    switch ((int) context.getId()) {
      case (int) IloCplex.Callback.Context.Id.ThreadUp:
        // The next if statement should be redundant; memory should always be
        // unset (subproblem.get() should always return null) in the ThreadUp
        // context.
        if (subproblem.get() == null) {
          if (showCalls) {
            System.out.println("$$$ Thread up occurred (thread "
                               + id + "/" + nth
                               + ") with no subproblem in place.");
          }
          Subproblem s = new Subproblem(problem, master, seed);
          s.setOut(subout);
          subproblem.set(s);
        } else if (showCalls) {
          System.out.println("$$$ Thread up occurred (thread "
                             + id + "/" + nth
                             + ") with a subproblem already in place.");
        }
        return;
      case (int) IloCplex.Callback.Context.Id.ThreadDown:
        // The next if statement should be redundant; memory should always be
        // set in the ThreadDown context.
        if (subproblem.get() == null) {
          if (showCalls) {
            System.out.println("$$$ Thread down occurred (thread "
                               + id + "/" + nth
                               + ") with no subproblem in place.");
          }
        } else {
          if (showCalls) {
            System.out.println("$$$ Thread down occurred (thread "
                               + id + "/" + nth
                               + ") with a subproblem in place.");
          }
          // Clear the subproblem.
          Subproblem s = subproblem.get();
          subproblem.set(null);
          s.end();
        }
        return;
      case (int) IloCplex.Callback.Context.Id.Candidate:
        // Make sure this thread has a copy of the subproblem. Again, this
        // should be redundant; the Candidate context should only occur
        // after the thread has seen a call in the ThreadUp context.
        if (subproblem.get() == null) {
          Subproblem s = new Subproblem(problem, master, seed);
          s.setOut(subout);
          subproblem.set(s);
          if (showCalls) {
            System.out.println("$$$ A candidate was supplied (thread "
                               + id + "/" + nth
                               + ") with no subproblem in place.");
          }
        } else if (showCalls) {
          System.out.println("$$$ A candidate was supplied (thread "
                             + id + "/" + nth
                             + ") with a subproblem already in place.");
        }
        // The master problem thinks it has an integer feasible solution.
        // Get the master flow cost estimate.
        double zMaster = master.getFlowCost(context);
        // Which warehouses does the proposed master solution use?
        double[] x = master.getWarehouseChoices(context);
        // Get the copy of the subproblem used in the current thread.
        Subproblem sub = subproblem.get();
        // Set the supply constraint right-hand sides in the subproblem.
        for (int w = 0; w < nWarehouses; w++) {
          sub.setRHS(w, x[w] >= Demo.ROUNDUP);
        }
        // Solve the subproblem.
        IloCplex.Status status = sub.solve();
        // Get an empty expression to use in building a cut.
        IloNumExpr expr = master.expression(0);
        if (status == IloCplex.Status.Infeasible) {
          // Subproblem is infeasible -- add a feasibility cut.
          // First step: get a Farkas certificate, corresponding to a dual ray
          // along which the dual is unbounded.
          IloConstraint[] constraints =
            new IloConstraint[nWarehouses + nCustomers];
          double[] coefficients = new double[nWarehouses + nCustomers];
          sub.dualFarkas(constraints, coefficients);
          // Process all elements of the Farkas certificate.
          for (int i = 0; i < constraints.length; i++) {
            IloConstraint c = constraints[i];
            expr = master.add(expr,
                              master.product(coefficients[i],
                                             sub.getRHSexpression(c)));
          }
          // Add a feasibility cut.
          IloRange r = master.makeCut(expr, 0, 'L');
          context.rejectCandidate(r);
          System.out.println("\n>>> Adding feasibility cut");
          if (showCuts) {
            System.out.println(r);
          }
          System.out.println();
        } else if (status == IloCplex.Status.Optimal) {
          if (zMaster < sub.getObjValue() - Solution.FUZZ) {
            // The master problem surrogate variable underestimates the actual
            // flow cost -- add an optimality cut.
            double[] lambda = sub.getDuals(true);  // demand constraints
            double[] mu = sub.getDuals(false);     // supply constraints
            // Compute the scalar product of the RHS of the demand constraints
            // with the duals for those constraints.
            for (int c = 0; c < nCustomers; c++) {
              expr = master.add(expr,
                                master.product(lambda[c], sub.getDemandRHS(c)));
            }
            // Repeat for the supply constraints.
            for (int w = 0; w < nWarehouses; w++) {
              expr = master.add(expr,
                                master.product(mu[w], sub.getSupplyRHS(w)));
            }
            // Add the optimality cut.
            IloRange r = master.makeCut(flowCost, expr, 'G');
            context.rejectCandidate(r);
            System.out.println("\n>>> Adding optimality cut");
            if (showCuts) {
              System.out.println(r);
            }
            System.out.println();
          } else {
            System.out.println("\n>>> Accepting new incumbent with value "
                               + context.getCandidateObjective() + "\n");
            // The master and subproblem flow costs match; if this is a
            // new incumbent (meaning no other thread has posted a better
            // solution), record the subproblem flows in case this proves
            // to be the winner (saving us from having to solve the LP one
            // more time once the master terminates).
            storeFlows(sub, context.getCandidateObjective());
          }
        } else {
          // An unexpected status occurred -- report it but do nothing.
          System.err.println("\n!!! Unexpected subproblem solution status: "
                             + status + "\n");
        }
        return;
      default:
        // The callback was somehow called from an invalid context. Print
        // an error message and punt.
        System.err.println("Callback was called from an invalid context: "
                           + context.getId() + ".\n");
    }
  }

  /**
   * Fill in the flow values in a solution.
   * @param solution the solution to complete
   */
  public void addFlows(final Solution solution) {
    for (int w = 0; w < nWarehouses; w++) {
      for (int c = 0; c < nCustomers; c++) {
        solution.setFlow(w, c, flowValues[w][c]);
      }
    }
  }

  /**
   * Test whether the master problem solution beats the current incumbent;
   * if so, update the incumbent value and store the flows from the subproblem
   * solution.
   *
   * Note: This method is synchronized to be sure that the incumbent value does
   * not change (courtesy of another thread) and result in this thread
   * overwriting the flows from a better solution.
   *
   * @param sub the subproblem for this thread
   * @param masterValue the objective value of the proposed new incumbent
   * @throws IloException if something goes wrong recovering the flows
   */
  private synchronized void storeFlows(final Subproblem sub,
                                       final double masterValue)
                            throws IloException {
    if (masterValue < incumbentValue) {
      incumbentValue = masterValue;
      for (int w = 0; w < nWarehouses; w++) {
        flowValues[w] = sub.getFlows(w);
      }
    }
  }
}
