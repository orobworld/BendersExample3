package threadsafe;

import bendersexample.Problem;
import ilog.concert.IloConstraint;
import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import java.io.OutputStream;
import java.util.HashMap;

/**
 * Subproblem holds a copy of the routing subproblem for the ManualBenders2
 * decomposition. Separate copies will be created for each thread.
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Subproblem {
  private final int nWarehouses;       // number of warehouses
  private final int nCustomers;        // number of customers
  private final IloCplex sub;          // the subproblem
  private final IloNumVar[][] ship;    // flow variables
  private final double[] capacity;     // warehouse capacities
  private final double[] demand;       // customer demands
  /*
   * To compute both optimality and feasibility cuts, we will need to multiply
   * the right-hand sides of the subproblem constraints (including both constant
   * terms and terms involving master problem variables) by the corresponding
   * subproblem dual values (obtained either directly, if the subproblem is
   * optimized, or via a Farkas certificate if the subproblem is infeasible).
   * To facilitate the computations, we will construct an instance of IloNumExpr
   * for each right-hand side, incorporating both scalars and master variables.
   *
   * Since CPLEX returns the Farkas certificate in no particular order (in
   * particular, NOT in the order the subproblem constraints are created),
   * we need to be able to access the IloNumExpr instances in the same arbitrary
   * order used by the Farkas certificate. To do that, we will store them in
   * a map keyed by the subproblem constraints themselves (which we will store
   * in arrays).
   */
  private final IloRange[] cSupply;                      // supply constraints
  private final IloRange[] cDemand;                      // demand constraints
  private final HashMap<IloConstraint, IloLinearNumExpr> rhs;
                                                         // right hand sides

  /**
   * Constructor.
   * @param problem the problem being solved
   * @param master the master problem associated with this subproblem
   * @param seed random seed for the solver
   * @throws IloException if the subproblem cannot be constructed
   */
  public Subproblem(final Problem problem, final MasterProblem master,
                    final int seed)
         throws IloException {
    nWarehouses = problem.getWarehouseCount();
    nCustomers = problem.getCustomerCount();
    rhs = new HashMap<>();
    // Record capacities and demands.
    capacity = new double[nWarehouses];
    demand = new double[nCustomers];
    for (int w = 0; w < nWarehouses; w++) {
      capacity[w] = problem.getCapacity(w);
    }
    for (int c = 0; c < nCustomers; c++) {
      demand[c] = problem.getDemand(c);
    }
    // Initialize the subproblem.
    sub = new IloCplex();
    // Set up the subproblem objective.
    ship = new IloNumVar[nWarehouses][nCustomers];
    IloLinearNumExpr expr = sub.linearNumExpr();
    for (int w = 0; w < nWarehouses; w++) {
      for (int c = 0; c < nCustomers; c++) {
        ship[w][c] = sub.numVar(0.0, Double.MAX_VALUE, "Flow_" + w + "_" + c);
        expr.addTerm(problem.getFlowCost(w, c), ship[w][c]);
      }
    }
    // Minimize total flow cost.
    sub.addMinimize(expr, "FlowCost");
    // Constrain demand to be satisfied -- record the constraints for use later.
    cDemand = new IloRange[nCustomers];
    for (int c = 0; c < nCustomers; c++) {
      expr.clear();
      for (int w = 0; w < nWarehouses; w++) {
        expr.addTerm(1.0, ship[w][c]);
      }
      cDemand[c] = sub.addGe(expr, demand[c], "Demand_" + c);
      rhs.put(cDemand[c], master.expression(demand[c]));
    }
    // Add supply limits (initially all zero, which makes the subproblem
    // infeasible):
    // -- record the constraints for use later;
    // -- also map each constraint to the corresponding binary variable in the
    //    master (for decoding Farkas certificates).
    cSupply = new IloRange[nWarehouses];
    for (int w = 0; w < nWarehouses; w++) {
      IloLinearNumExpr xpr = master.term(capacity[w], w);
      cSupply[w] = sub.addLe(sub.sum(ship[w]), 0.0, "Supply_" + w);
      rhs.put(cSupply[w], xpr);
    }
    // Disable presolving of the subproblem (if the presolver recognizes that
    // the subproblem is infeasible, we do not get a dual ray).
    sub.setParam(IloCplex.BooleanParam.PreInd, false);
    // Force use of the dual simplex algorithm to get a Farkas certificate.
    // (The dualFarkas method requires that the dual simplex algorithm be used.)
    sub.setParam(IloCplex.IntParam.RootAlg, 2);
    // Suppress master and subproblem output by default.
    sub.setOut(null);
    // Set the seed for the solver.
    sub.setParam(IloCplex.IntParam.RandomSeed, seed);
  }

  /**
   * Set the output stream for the subproblem.
   * @param os the new output stream
   */
  public void setOut(final OutputStream os) {
    sub.setOut(os);
  }

  /**
   * Set the random seed for the solver.
   * @param seed the new seed
   * @throws IloException if CPLEX balks
   */
  public void setSeed(final int seed) throws IloException {
    sub.setParam(IloCplex.IntParam.RandomSeed, seed);
  }

  /**
   * Adjust the upper bound on the capacity of a warehouse according to
   * whether the warehouse is used or not.
   * @param warehouse the warehouse index
   * @param use true if the warehouse is used, false if not
   * @throws IloException if CPLEX cannot/will not change the upper bound
   */
  public void setRHS(final int warehouse, final boolean use)
              throws IloException {
    cSupply[warehouse].setUB((use) ? capacity[warehouse] : 0);
  }

  /**
   * Solve the subproblem and return the solver status.
   * @return the final solver status
   * @throws IloException if CPLEX blows up solving the problem
   */
  public IloCplex.Status solve() throws IloException {
    sub.solve();
    return sub.getStatus();
  }

  /**
   * Get a dual Farkas certificate.
   * @param constraints the vector of constraints (output)
   * @param values the vector of dual values (output)
   * @throws IloException if CPLEX balks
   */
  public void dualFarkas(final IloConstraint[] constraints,
                         final double[] values)
              throws IloException {
    sub.dualFarkas(constraints, values);
  }

  /**
   * Get the master problem expression tied to a particular constraint.
   * @param constraint the subproblem constraint
   * @return the corresponding master problem expression
   */
  public IloLinearNumExpr getRHSexpression(final IloConstraint constraint) {
    return rhs.get(constraint);
  }

  /**
   * Get the master problem expression tied to a demand constraint.
   * @param c the index of the constraint (customer)
   * @return the corresponding master problem expression
   */
  public IloLinearNumExpr getDemandRHS(final int c) {
    return rhs.get(cDemand[c]);
  }

  /**
   * Get the master problem expression tied to a supply constraint.
   * @param w the index of the constraint (warehouse)
   * @return the corresponding master problem expression
   */
  public IloLinearNumExpr getSupplyRHS(final int w) {
    return rhs.get(cSupply[w]);
  }

  /**
   * Get the subproblem objective value.
   * @return the subproblem objective value
   * @throws IloException if CPLEX encounters a hitch
   */
  public double getObjValue() throws IloException {
    return sub.getObjValue();
  }

  /**
   * Get the dual multipliers for selected constraints.
   * @param demands if true, get duals for the demand constraints, else for the
   * supply constraints
   * @return the dual multipliers for those constraints
   * @throws IloException if CPLEX burps
   */
  public double[] getDuals(final boolean demands) throws IloException {
    if (demands) {
      return sub.getDuals(cDemand);
    } else {
      return sub.getDuals(cSupply);
    }
  }

  /**
   * Get the flow values for all shipments out of a warehouse.
   * @param w the warehouse index
   * @return the vector of shipment values
   * @throws IloException if CPLEX hiccups
   */
  public double[] getFlows(final int w) throws IloException {
    return sub.getValues(ship[w]);
  }

  /**
   * Clean up all CPLEX objects.
   */
  public void end() {
    sub.end();
  }
}
