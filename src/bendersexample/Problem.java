package bendersexample;

import java.util.Random;

/**
 * Problem encapsulates a randomly generated problem instance.
 * Key parameters for generating the problem are coded as class constants.
 * Modify them to alter the size/difficulty of the problem.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Problem {
  /* Configure model dimensions here */
  private static final int WAREHOUSES = 50;     // number of warehouses
  private static final int CUSTOMERS = 4000;    // number of customers
  private static final double CAPMULT = 2.5;    // mean capacity multiplier
  private static final double FIXEDCOST = 500;  // mean fixed cost/warehouse

  private final double[] demand;                // customer demands
  private final double[] capacity;              // warehouse capacities
  private final double[] fixedCost;             // fixed costs of warehouses
  private final double[][] flowCost;            // unit flow costs from
                                                // warehouses to customers

  /**
   * Constructor.
   *
   * @param seed the seed for the random number generator
   */
  public Problem(final long seed) {
    // Seed the random number generator.
    Random rng = new Random(seed);
    // Generate customer demands.
    demand = new double[CUSTOMERS];
    double totalDemand = 0;
    for (int i = 0; i < CUSTOMERS; i++) {
      demand[i] = rng.nextDouble();
      totalDemand += demand[i];
    }
    // Set the mean capacity of any one warehouse.
    double meanCapacity = CAPMULT * totalDemand / WAREHOUSES;
    // Set the capacities and fixed costs of individual warehouses.
    capacity = new double[WAREHOUSES];
    fixedCost = new double[WAREHOUSES];
    for (int i = 0; i < WAREHOUSES; i++) {
      capacity[i] = meanCapacity * 2 * rng.nextDouble();
      fixedCost[i] = 2 * FIXEDCOST * rng.nextDouble();
    }
    flowCost = new double[WAREHOUSES][CUSTOMERS];
    for (int i = 0; i < WAREHOUSES; i++) {
      for (int j = 0; j < CUSTOMERS; j++) {
        flowCost[i][j] = rng.nextDouble();
      }
    }
  }

  /**
   * Get the number of warehouses.
   * @return the number of warehouses
   */
  public int getWarehouseCount() {
    return WAREHOUSES;
  }

  /**
   * Get the number of customers.
   * @return the number of customers
   */
  public int getCustomerCount() {
    return CUSTOMERS;
  }

  /**
   * Get the fixed cost of a warehouse.
   * @param w the index of the warehouse
   * @return the fixed cost of the warehouse
   */
  public double getFixedCost(final int w) {
    return fixedCost[w];
  }

  /**
   * Get the capacity of a warehouse.
   * @param w the index of the warehouse
   * @return the capacity of the warehouse
   */
  public double getCapacity(final int w) {
    return capacity[w];
  }

  /**
   * Get the demand of a customer.
   * @param c the index of the customer
   * @return the demand of the customer
   */
  public double getDemand(final int c) {
    return demand[c];
  }

  /**
   * Get the unit flow cost of an arc.
   * @param w the index of the warehouse
   * @param c the index of the customer
   * @return the unit flow cost from the warehouse to the customer
   */
  public double getFlowCost(final int w, final int c) {
    return flowCost[w][c];
  }
}
