package bendersexample;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import java.util.HashSet;

/**
 * AnnotatedBenders uses the new features in CPLEX 12.7 to solve the problem
 * via Benders decomposition. The user annotates the variables to indicate
 * which go in the master problem and which go in each subproblem.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class AnnotatedBenders implements Model {
  private final IloCplex cplex;   // the MIP model
  private final IloNumVar[] use;  // use[i] = 1 if warehouse i is used, 0 if not
  private final IloNumVar[][] ship;
                                  // ship[i][j] is the flow from warehouse i
                                  // to customer j
  private final int nWarehouses;  // number of warehouses
  private final int nCustomers;   // number of customers

  /**
   * Constructor.
   *
   * Construction is identical to construction of the StandardModel. The only
   * difference is the addition of annotations after the model is built.
   *
   * @param problem the problem to solve
   * @param seed random seed value
   * @throws IloException if model construction fails
   */
  public AnnotatedBenders(final Problem problem, final int seed)
         throws IloException {
    nWarehouses = problem.getWarehouseCount();
    nCustomers = problem.getCustomerCount();
    cplex = new IloCplex();
    use = new IloNumVar[nWarehouses];
    ship = new IloNumVar[nWarehouses][nCustomers];
    IloLinearNumExpr expr = cplex.linearNumExpr();
    // Declare the variables and simultaneously assemble the objective function.
    for (int w = 0; w < nWarehouses; w++) {
      use[w] = cplex.boolVar("Use" + w);
      expr.addTerm(problem.getFixedCost(w), use[w]);
      for (int c = 0; c < nCustomers; c++) {
        ship[w][c] = cplex.numVar(0.0,
                                  Math.min(problem.getCapacity(w),
                                           problem.getDemand(c)),
                                  "Ship_" + w + "_" + c);
        expr.addTerm(problem.getFlowCost(w, c), ship[w][c]);
      }
    }
    cplex.addMinimize(expr, "TotalCost");  // minimize total cost
    // Add demand constraints.
    for (int c = 0; c < nCustomers; c++) {
      expr.clear();
      for (int w = 0; w < nWarehouses; w++) {
        expr.addTerm(1.0, ship[w][c]);
      }
      cplex.addGe(expr, problem.getDemand(c), "Demand_" + c);
    }
    // Add supply constraints.
    for (int w = 0; w < nWarehouses; w++) {
      cplex.addLe(cplex.sum(ship[w]),
                  cplex.prod(problem.getCapacity(w), use[w]),
                  "Supply_" + w);
    }
    /*
      To specify the decomposition manually, we first need to create an
      instance of IloCplex.LongAnnotation with a specific name
      ("cpxBendersPartition"). The name of the variable in which you store it is
      immaterial. We then assign a value of that annotation to each variable,
      where 0 indicates that the variable belongs in the master problem and
      k > 0 indicates that the variable belongs in the k-th subproblem. If we
      fail to annotate a variable, the problem it goes into is determined by
      the default value we specified when declaring the annotation. Logical
      choices might be 0 (variables default to the master problem), 1
      (variables default to the first and only subproblem) or -1 (which triggers
      an exception -- I would use this during debugging to make sure that every
      variable was annotated).

      Below we assign explicit annotations to every variable for illustration
      purposes. It would be simpler (and faster?), however, to set the default
      value to 1 and not bother to annotate the "ship" variables, which would
      then default to subproblem 1.
    */
    // Create an annotation with default value 0L.
    IloCplex.LongAnnotation benders =
      cplex.newLongAnnotation("cpxBendersPartition");
    // Put the binary "use" variables in the master problem.
    for (IloNumVar u : use) {
      cplex.setAnnotation(benders, u, 0);
    }
    // The LP portion does not decompose into smaller problems, so we put all
    // the "ship" variables in subproblem 1.
    for (IloNumVar[] s : ship) {
      for (IloNumVar s0 : s) {
        cplex.setAnnotation(benders, s0, 1);
      }
    }
    // Set the default Benders strategy to be adherence to our design.
    cplex.setParam(IloCplex.Param.Benders.Strategy,
                   IloCplex.BendersStrategy.User);
    // Suppress output.
    cplex.setOut(null);
    // Set the seed.
    cplex.setParam(IloCplex.IntParam.RandomSeed, seed);
  }

  /**
   * Set the Benders strategy for the model.
   *
   * This lets us override the strategy specified in the constructor.
   *
   * @param strategy the new Benders strategy
   * @throws IloException if CPLEX balks at setting the strategy
   */
  @Override
  public void setBendersStrategy(final int strategy) throws IloException {
    cplex.setParam(IloCplex.Param.Benders.Strategy, strategy);
  }

  /**
   * Solve the model.
   * @return the solution (in an instance of Solution)
   * @throws IloException if CPLEX encounters problems
   */
  @Override
  public Solution solve() throws IloException {
    Solution s = new Solution(nWarehouses, nCustomers);
    long start = System.currentTimeMillis();
    if (cplex.solve()) {
      // Fill in the solution.
      s.setTime(System.currentTimeMillis() - start);
      s.setCost(cplex.getObjValue());
      s.setStatus(cplex.getCplexStatus());
      HashSet<Integer> warehouses = new HashSet<>();
      double[] u = cplex.getValues(use);
      for (int w = 0; w < use.length; w++) {
        double[] flows = cplex.getValues(ship[w]);
        for (int c = 0; c < nCustomers; c++) {
          s.setFlow(w, c, flows[c]);
        }
        if (u[w] > Demo.ROUNDUP) {
          warehouses.add(w);
        }
      }
      s.setWarehouses(warehouses);
    } else {
      // Record only run time and status in the solution.
      s.setTime(System.currentTimeMillis() - start);
      s.setStatus(cplex.getCplexStatus());
    }
    return s;
  }

  /**
   * Set the output level for the solver.
   *
   * @param level the desired output level
   * (0 = none [default], 1 = master only, 2 = master and subproblems)
   */
  @Override
  public void setOutputLevel(final int level) {
    switch (level) {
      case 0:  // suppress all output
        cplex.setOut(null);
        break;
      case 1:  // master output only
      case 2:  // master and subproblem output (which is just master in
               // this case)
        cplex.setOut(System.out);
        break;
      default: // illegal argument
        throw new IllegalArgumentException("Invalid output level specified.");
    }
  }

  /**
   * Set the random number seed for the solver.
   * @param seed the seed to use
   * @throws IloException if the seed cannot be set
   */
  @Override
  public void setSeed(final int seed) throws IloException {
    cplex.setParam(IloCplex.IntParam.RandomSeed, seed);
  }

  /**
   * Clean up any objects attached to the model.
   *
   */
  @Override
  public void end() {
    cplex.end();
  }
}
