package bendersexample;

import static bendersexample.Solution.MSTOSEC;
import bendersexample.Solution.Verbosity;
import ilog.concert.IloException;
import ilog.cplex.IloCplex;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import threadsafe.ManualBenders2;

/**
 * Demo executes the code demonstrating Benders decomposition in CPLEX (12.7
 * or later).
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 * @version 1.0
 */
public final class Demo {
  /**
   * CALLS modifies the output level to trigger messages whenever the Benders
   * callback is entered (ManualBenders2 only).
   */
  public static final int CALLS = 10;
  /** ROUNDUP is the cutoff for rounding values of binary variables up. */
  public static final double ROUNDUP = 0.5;
  private static final int DEFAULTSEED = 72612;  // default random seed
  private static final int NUMBEROFTRIALS = 1;   // default number of trials
  /**
   * ModelType enumerates the possible types of model to run.
   *
   * STANDARD = single MIP model (no decomposition).
   * MANUAL = master plus one subproblem, hard-coded.
   * MANUAL2 = master plus one subproblem, using thread-local copies of the
   * subproblem
   * ANNOTATED = master plus one subproblem, created using annotations.
   * AUTOMATIC = master plus one or more subproblems, created from a standard
   * model by CPLEX.
   * WORKERS = annotated model giving CPLEX the option to decompose the
   * subproblem into multiple disjoint subproblems.
  */
  private enum ModelType { STANDARD, MANUAL, MANUAL2, ANNOTATED,
                           AUTOMATIC, WORKERS };

  /**
   * Constructor.
   */
  private Demo() { }

  /**
   * Main method.
   *
   * The program recognizes a number of command line options. Run it with
   * -h or --help to see a usage message.
   *
   * Note: We request garbage collection after each model is tried, in an
   * attempt to ensure that run times for later models are not stretched
   * because the later models are squeezed for memory.
   *
   * @param args the command line arguments (not used)
   */
  public static void main(final String[] args) {
    // Set up an object to hold command line options, and populate it.
    Options options = new Options();
    populateOptions(options);
    // Parse the command line.
    CommandLineParser parser = new DefaultParser();
    CommandLine line = null;
    try {
      line = parser.parse(options, args);
    } catch (ParseException ex) {
      System.err.println("Unable to parse the command line:\n"
                         + ex.getMessage());
      System.exit(1);
    }
    // If the user asked for help, display it and punt.
    if (line.hasOption("h")) {
      HelpFormatter formatter = new HelpFormatter();
      formatter.printHelp("java -Djava.library.path=<path to CPLEX binary>"
                          + " -jar BendersExample2.jar [options]", options);
      System.exit(0);
    }
    // Set the number of trials, random seed and verbosity parameters based
    // on the command line inputs.
    int seed = parseIntegerOption(line, "s", Integer.MIN_VALUE,
                                  Integer.MAX_VALUE, DEFAULTSEED);
    int nTrials = parseIntegerOption(line, "t", 0, Integer.MAX_VALUE,
                                     NUMBEROFTRIALS);
    int outLevel = parseIntegerOption(line, "o", 0, 2, 0);
    boolean showCuts = line.hasOption("c");
    int v = parseIntegerOption(line, "v", 0, 2, 0);
    Verbosity verbosity = Verbosity.values()[v];
    for (int trial = 0; trial < nTrials; trial++) {
      System.out.println("\n\n+++++++++++++++++++");
      System.out.println("Trial " + trial + "\n");
      // Generate the problem instance.
      Problem problem = new Problem(seed);
      if (line.hasOption("std") || line.hasOption("a")) {
        // Set up and solve a standard MIP model, to serve as a benchmark.
        runModel(problem, ModelType.STANDARD, "Standard Model",
                 outLevel, verbosity, showCuts, seed);
      }
      if (line.hasOption("m") || line.hasOption("a")) {
        // Set up and solve a "traditional" Benders decomposition (separate
        // master and subproblem).
        runModel(problem, ModelType.MANUAL, "Manual Decomposition",
                 outLevel, verbosity, showCuts, seed);
      }
      if (line.hasOption("m2") || line.hasOption("a")) {
        // See if the user asked for additional output (callback entry
        // messages).
        int extra = (line.hasOption("sc")) ? CALLS : 0;
        // Run the more complex version of the "traditional" decomposition,
        // using separate copies of the subproblem in each thread.
        runModel(problem, ModelType.MANUAL2, "Thread-Local Decomposition",
                 outLevel + extra, verbosity, showCuts, seed);
      }
      if (line.hasOption("auto") || line.hasOption("a")) {
        // Use the "full Benders" feature to automatically implement Benders
        // decomposition on a standard MIP model.
        runModel(problem, ModelType.AUTOMATIC, "Automatic Decomposition",
                 outLevel, verbosity, showCuts, seed);
      }
      if (line.hasOption("ann") || line.hasOption("a")) {
        // Use an annotated model with a single subproblem.
        runModel(problem, ModelType.ANNOTATED, "Annotated Decomposition",
                 outLevel, verbosity, showCuts, seed);
      }
      if (line.hasOption("w") || line.hasOption("a")) {
        // Use the annotated model letting CPLEX look for further
        // decomposition of the subproblems.
        runModel(problem, ModelType.WORKERS, "Workers Decomposition",
                 outLevel, verbosity, showCuts, seed);
      }
      seed += 1;
      System.out.println("\n+++++++++++++++++++");
    }
  }

  /**
   * Run a model and print the results.
   *
   * @param problem the problem to solve
   * @param type the type of model to run
   * @param title the title to use in the output
   * @param outputLevel 0 to suppress solver output, 1 to show master problem
   * output only, 2 to show both master and subproblem output
   * @param verbosity the desired verbosity of the solution report
   * @param showCuts manual decomposition only: show generated cuts?
   * @param seed random seed to be used by CPLEX
   */
  private static void runModel(final Problem problem, final ModelType type,
                               final String title, final int outputLevel,
                               final Verbosity verbosity,
                               final boolean showCuts,
                               final int seed) {
    Model model;
    System.out.println("\n==========\nRunning " + title);
    long time = System.currentTimeMillis();
    try {
      switch (type) {
        case MANUAL:
          model = new ManualBenders(problem, showCuts, seed);
          break;
        case MANUAL2:
          model = new ManualBenders2(problem, showCuts, seed);
          break;
        case ANNOTATED:
          model = new AnnotatedBenders(problem, seed);
          break;
        case STANDARD:
          model = new StandardModel(problem, seed);
          break;
        case AUTOMATIC:
          model = new StandardModel(problem, seed);
          model.setBendersStrategy(IloCplex.BendersStrategy.Full);
          break;
        case WORKERS:
          model = new AnnotatedBenders(problem, seed);
          model.setBendersStrategy(IloCplex.BendersStrategy.Workers);
          break;
        default:
          throw new IllegalArgumentException("Invalid model type specified.");
      }
    } catch (IloException ex) {
      System.out.println("Failed to build the model:\n" + ex.getMessage());
      return;
    }
    // Record the time to build the model
    time = System.currentTimeMillis() - time;
    System.out.println("Time to build model = " + MSTOSEC * time + " secs.");
    // Set the output level.
    model.setOutputLevel(outputLevel);
    Solution s;
    try {
      s = model.solve();
      System.out.println(s.report(verbosity));
    } catch (IloException ex) {
      System.out.println("Solution failed:\n" + ex.getMessage());
    }
    // Clean up.
    model.end();
    model = null;
    System.gc();
  }

  /**
   * Populate the command line options object.
   * @param options the options object to populate
   */
  private static void populateOptions(final Options options) {
    // help
    options.addOption("h", "help", false, "print this help message and quit");
    // general options
    options.addOption("c", "cuts", false, "show cut details (Benders models)");
    options.addOption("o", "output", true,
                      "model output level (0 = none, 1 = master only, "
                      + "2 = master and subproblem)");
    Option o = options.getOption("o");
    o.setArgName("int");
    o.setType(Number.class);
    options.addOption("sc", "show-calls", false,
                      "print a message when the callback is called (manual2"
                      + " method only)");
    options.addOption("s", "seed", true, "specify a random number seed (int)");
    Option s = options.getOption("s");
    s.setArgName("int");
    s.setType(Number.class);
    options.addOption("t", "trials", true, "number of trials to run");
    Option t = options.getOption("t");
    t.setArgName("int");
    t.setType(Number.class);
    options.addOption("v", "verbosity", true, "solution verbosity (0-2)");
    Option v = options.getOption("verbosity");
    v.setArgName("int");
    v.setType(Number.class);
    // method selection
    options.addOption("a", "all", false, "run all models");
    options.addOption("ann", "annotated", false,
                      "run the annotated Benders model");
    options.addOption("auto", "automatic", false,
                      "run the automatic Benders model");
    options.addOption("m", "manual", false, "run the manual model");
    options.addOption("m2", "manual2", false,
                      "run the thread-local manual model");
    options.addOption("std", "standard", false, "run the standard MIP model");
    options.addOption("w", "workers", false, "run the workers Benders model");
  }

  /**
   * Parse the value of an integer command line option.
   * @param line the command line
   * @param name the short name for the option
   * @param min the minimum legal value for the option
   * @param max the maximum legal value for the option
   * @param defaultValue the default value for the option
   * @return the value of the option
   */
  private static int parseIntegerOption(final CommandLine line,
                                        final String name,
                                        final int min, final int max,
                                        final int defaultValue) {
    if (line.hasOption(name)) {
      try {
        int value = ((Number) line.getParsedOptionValue(name)).intValue();
        if (value >= min && value <= max) {
          return value;
        } else {
          System.err.println("Value " + value + " for option " + name
                             + " is outside the legal range [" + min
                             + ", " + max + "].");
          System.exit(1);
          return 0;
        }
      } catch (ParseException ex) {
        System.err.println("Unable to parse command line argument "
                           + name + " to integer.");
        System.exit(1);
        return 0;
      }
    } else {
      return defaultValue;
    }
  }

}
