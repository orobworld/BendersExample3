package bendersexample;

import ilog.concert.IloException;

/**
 * Model provides a uniform interface for solving models of the test problem.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public interface Model {
  /**
   * Set the Benders strategy for the model.
   *
   * This lets us override the strategy specified in the constructor, if any.
   *
   * @param strategy the new Benders strategy
   * @throws IloException if CPLEX balks at setting the strategy
   */
  void setBendersStrategy(final int strategy) throws IloException;

  /**
   * Solve the model.
   *
   * @return the solution (in an instance of Solution)
   * @throws IloException if CPLEX encounters problems
   */
  Solution solve() throws IloException;

  /**
   * Set the output level for the solver.
   *
   * @param level the desired output level
   * (0 = none [default], 1 = master only, 2 = master and subproblems)
   */
  void setOutputLevel(final int level);

  /**
   * Set the random number seed for the solver.
   * @param seed the seed to use
   * @throws IloException if the seed cannot be set
   */
  void setSeed(final int seed) throws IloException;

  /**
   * Clean up any objects attached to the model.
   *
   */
  void end();
}
